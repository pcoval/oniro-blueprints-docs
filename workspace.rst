.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ./definitions.rst

.. _Workspace:

Workspace
#########

.. contents::
   :depth: 2

Oniro Project Workspace
***********************

In order to be able to build a blueprints, a set up workspace is required. The
blueprints require a `oniro` workspace so the first step is to follow the
instructions available on `Oniro Project documentation
<https://docs.oniroproject.org/en/latest/oniro/repo-workspace.html>`_

If you already have a workspace in place, make sure you update it by running
`repo sync`.

Blueprints Workspace
********************

A blueprint build is very similar to an `oniro` build but requires additional
layers set up in the build. The general steps are:

#. Define a place for the blueprints layer and its dependencies

   .. code-block:: bash

      $ export CHECKOUT_DIR=~/oniroproject
      $ mkdir -p $CHECKOUT_DIR


#. Clone the layer for the blueprint which you want to add:

   .. code-block:: bash

      $ cd $CHECKOUT_DIR
      $ git clone --recursive https://gitlab.eclipse.org/eclipse/oniro-blueprints/<name-of-the-blueprint>/meta-oniro-blueprints-<name-of-the-blueprint>
      $ cd meta-oniro-blueprints-<name-of-the-blueprint>

#. The first time a build is initialized, after running the `oe-init-build-env`
   script, add all layers present in `meta-oniro-blueprints-<name-of-the-blueprint>`
   to the build's `bblayers.conf` either manually or by
   running (for each layer separately):

   .. code-block:: bash

      $ bitbake-layers add-layer $CHECKOUT_DIR/meta-oniro-blueprints-<name-of-the-blueprint>/<name-of-a-layer>

.. note::
   If 'oniro-blueprints-core' layer exists in blueprint directory, you may need to add it first.


.. attention::

   Make sure you define (as per the instructions above) or replace
   `CHECKOUT_DIR` accordingly.

#. Once that is done. Follow the specific blueprint's build instructions.
